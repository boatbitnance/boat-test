FROM node:16.17.0

WORKDIR /app
COPY . /app
COPY package.json ./

RUN npm install

EXPOSE 8081

CMD ["node", "index.js"]